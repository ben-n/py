# Environment Variables

## Install dependency

```python
pip install python-dotenv
```

## File

1. create .env file with environment variables

## Code

```python
import os
from dotenv import load_dotenv

load_dotenv()

API_KEY = os.getenv('MY_API_KEY')
```

## Notes

See [https://github.com/theskumar/python-dotenv#variable-expansion](https://github.com/theskumar/python-dotenv#variable-expansion)
`load_dotenv(override=True)` will allow .env files to override .bashrc
