# Logging
https://docs.python.org/3/library/logging.html

|           Level          | Numeric value |                                                                                              What it means / When to use it                                                                                              |
|:-------------------------|:-------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|    logging.NOTSET        | 0             | When set on a logger, indicates that ancestor loggers are to be consulted to determine the effective level. If that still resolves to NOTSET, then all events are logged. When set on a handler, all events are handled. |
|    logging.DEBUG         | 10            | Detailed information, typically only of interest to a developer trying to diagnose a problem.                                                                                                                            |
|    logging.INFO          | 20            | Confirmation that things are working as expected.                                                                                                                                                                        |
|    logging.WARNING       | 30            | An indication that something unexpected happened, or that a problem might occur in the near future (e.g. ‘disk space low’). The software is still working as expected.                                                   |
|    logging.ERROR         | 40            | Due to a more serious problem, the software has not been able to perform some function.                                                                                                                                  |
|    logging.CRITICAL      | 50            | A serious error, indicating that the program itself may be unable to continue running.                                                                                                                                   |


```python
import logging

def main() -> None:
    logging.basicConfig(
        level=logging.DEBUG,  # debug, info, warning, error, critical
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        filename="basic.log",
    )

    logging.debug("This is a debug message.")
    logging.info("This is an info message.")
    logging.warning("This is a warning message.")
    logging.error("This is an error message.")
    logging.critical("This is a critical message.")
    return


if __name__ == "__main__":
    main()

```