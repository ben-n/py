# Virtual Environments
[https://docs.python.org/3/tutorial/venv.html](https://docs.python.org/3/tutorial/venv.html)

Do not move virtual environment after creation. Package links will not work after move.

## Create an environment
```
python -m venv NAME
```

## Activation
Unix / MacOS
```
source ~/.venv/NAME/bin/activate
```
Windows
```
NAME\Scripts\activate
```

## Deactivation
```
deactivate
```