# How to add an AppImage to the application menu

1. Download AppImage
2. chmod +x .appimage
3. mv .appimage ~/bin/<name>
4. $ ~/bin/<name>
5. $ mount | grep <name>
6. cd <path>
7. cp ICON ~/.local/share/icons
8. cp .desktop ~/.local/share/applications
9. edit .desktop file, and update Exec= line

