# Benjamin Nicholls (2024)
# 25481282@students.lincoln.ac.uk

import argparse
import os
import logging


DIR_PATH = os.path.dirname(os.path.realpath(__file__))


def config():
    # Logging.
    logging.basicConfig(
        level=logging.DEBUG,  # debug, info, warning, error, critical
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        #filename="basic.log",
    )
    return


def parse_arguments():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--task1',
                        help='Show output for task 1? [0/1]',
                        dest='task1',
                        default=False,
                        action='store_true')
    args = parser.parse_args()
    return args


def main(task1, task2) -> None:
    return


if __name__ == "__main__":
    config()
    args = parse_arguments()
    main(bool(args.task1), bool(args.task2))
    logging.debug('Program end.')
