# Python

[Template](./template.py)

A collection of useful notes on:
- [Python Style Guide](./python-style-guide.md)
- [Logging](logging.md)
- [Virtual Envrionments](./venv.md)
- [.env files](./dotenv.md)
- [argparse](./argparse.md)
- [Clean up ignored files after commit](./ignore-committed-files.md)

Links:
- [gitignores](https://github.com/github/gitignore)