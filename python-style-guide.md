# Python Style Guide
From _https://peps.python.org/pep-0008/_

## Comments
- Docstrings should use "", not ''
```python
"""Return a foobang

Optional blah blah blah.
"""
```
or one-liner
```python
"""Return a foobang."""
```

## Formatting
- Maximum line length = 79 characters
- Docstrings/comment maximum line length = 72 characters

- Surround top-level function and class definitions with two blank lines.
- Method definitions inside a class are surrounded by a single blank line.

## Spacing
- 4 spaces for indentation
- binary operators should have a space on both sides (=, +=, !=, ==, <, is, etc.)
- do not include a space when = is used to indicate a keyword argument
- but when an argument is combined with a default value, do use a space
```python
def munge(input: AnyStr, sep: AnyStr = None, limit=1000): ...
```

## Naming
- Do not use l or o, just use L and O
- Modules: short, all-lowercase names (underscores only used for readability)
- Packages: short, all-lowercase names (no underscores unless C package)
- Class names: CapWords
- Exception names: CapWords + 'Error'
- Function names: all-lowercase with underscores
- Variable names: all-lowercase with underscores
- Constant names: CAPS

## Arguments
- self is first argument to instance methods
- cls is first argument to class methods